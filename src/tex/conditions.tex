\subsection{Syntaxe}

\begin{frame}[fragile]{Introduction}
	\begin{block}{Définition}
		On appelle {\bf condition} ou {\bf structure conditionnelle} un morceau de code qui est exécuté uniquement dans certains cas.
	\end{block}
	En algorithmique, on les voit souvent sous la forme :
\medskip
	\begin{Verbatim}
{\large SI condition}

{\large ALORS}

	{\large actions}

{\large FIN_SI}
	\end{Verbatim}
\end{frame}

\begin{frame}[fragile]{Et en Bash ?}
	En Bash, la structure à utiliser est la suivante :
	\medskip
	\begin{Verbatim}
{\large if [ condition ]}

{\large then}

	{\large actions}

{\large fi}
	\end{Verbatim}
	\begin{alertblock}{Attention !}
		Les espaces entre les \verb+[]+ et la condition sont \textbf{indispensables}.
	\end{alertblock}
	\begin{block}{}
		\begin{itemize}
			\item À noter : \texttt{fi}, c'est \texttt{if} à l'envers !
			\item On verra plus tard quelles sont les conditions que l'on peut utiliser.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}[fragile]{Si~\ldots sinon~\ldots}
	\begin{block}{Contexte}
		La structure précédente permet de réaliser des actions si une condition est évaluée à \textbf{vrai}.
		On rencontre souvent une autre structure, qui dit aussi quoi faire si la condition est évaluée à \textbf{faux} :
	\end{block}
	\begin{Verbatim}

{\large SI condition}

{\large ALORS}

	{\large actions}

{\large SINON}

	{\large actions}

{\large FIN_SI}
	\end{Verbatim}
\end{frame}

\begin{frame}[fragile]{En Bash~\ldots}
	\begin{block}{}
	En Bash, on utilise la même syntaxe, et on utilise \enquote{\texttt{else}} comme mot clé pour le \enquote{sinon}.
	\end{block}
	\medskip
	\begin{Verbatim}
{\large if [ condition ]}

{\large then}

	{\large actions}

{\large else}

	{\large actions}

{\large fi}
	\end{Verbatim}
\end{frame}

\begin{frame}[fragile]{Si \ldots sinon si \ldots sinon}
	\begin{block}{}
		\enquote{Et si je veux faire encore plus compliqué ?}
	\end{block}
	Il existe encore un autre moyen d'écrire des \texttt{SI} :
	\begin{Verbatim}

{\large SI condition}

{\large ALORS}

	{\large actions}

{\large SINON SI condition}

{\large ALORS}

	{\large actions}

{\large SINON}

	{\large actions}

{\large FIN_SI}
	\end{Verbatim}

	\begin{alertblock}{}
	On peut mettre autant de \enquote{SINON SI \ldots ALORS \ldots} que l'on veut !
	\end{alertblock}
\end{frame}

\begin{frame}[fragile]{En Bash}
	\begin{block}{}
	En bash, \enquote{SINON SI} s'écrit \texttt{elif} (contraction de \texttt{else} et \texttt{if}) :
	\end{block}

	\medskip
	\begin{Verbatim}
{\large if [ condition ]}

{\large then}

	{\large actions}

{\large elif [ condition ]}

{\large then}

	{\large actions}

{\large else}

	{\large actions}

{\large fi}
	\end{Verbatim}
\end{frame}

\subsection{Les conditions}

\begin{frame}[fragile]{Chaînes de caractères}{Les différents tests}
	\begin{table}
		\centering
		\begin{tabularx}{\linewidth}{|X|X|}
			\hline
			\textbf{Test et syntaxe} & \textbf{Explications} \\
			\hline\hline
			\verb+"$chaine1" == "$chaine2"+ & Teste si \verb+$chaine1+ et \verb+$chaine2+ sont \textbf{égaux}\\
			\hline
			\verb+"$chaine1" != "$chaine2"+ & Teste si \verb+$chaine1+ et \verb+$chaine2+ sont \textbf{différents} \\
			\hline
			\verb+-z $chaine+ & Teste si la la variable \verb+$chaine+ est \textbf{vide} \\
			\hline
			\verb+-n $chaine+ & Teste si la variable \verb+$chaine+ est \textbf{non vide} \\
			\hline
		\end{tabularx}
		\caption{Les tests sur les chaînes de caractères}
		\label{tab:tests_chaines}
	\end{table}
\end{frame}

\begin{frame}[fragile]{Chaînes de caractères}{Exemples}
	Dans \verb+script.sh+, on écrit :
	\medskip
	
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash

if [ "$1" == "mot_de_passe" ]
then
    echo 'Bienvenue'
elif [ "$1" == "password" ]
then
    echo 'Welcome !'
else
    echo 'Raté !'
fi

if [ -z $2 ]
then
    echo "Il n'y a pas de second argument"
fi
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]
	L'exécution du script donne :
	\medskip
	
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh \Pause
Raté !
Il n'y a pas de second argument \Pause
remy@hp-remy:~/scripts$ ./script.sh mot_de_passe \Pause
Bienvenue
Il n'y a pas de second argument \Pause
remy@hp-remy:~/scripts$ ./script.sh password toto \Pause
Welcome !
		\end{Verbatim}
	\end{beamercolorbox}
	\medskip

\end{frame}

\begin{frame}[fragile]{Nombres}{Les différents tests}
	\begin{table}
		\centering
		\begin{tabular}{|c|c|}
			\hline
			\textbf{Test et syntaxe} & \textbf{Explications} \\
			\hline\hline
			\verb+$a -eq $b+ & Test d'égalité \\
			\hline
			\verb+$a -ne $b+ & Test d'inégalité \\
			\hline
			\verb+$a -lt $b+ & \verb+$a+ inférieur à \verb+$b+ \\
			\hline
			\verb+$a -le $b+ & \verb+$a+ inférieur ou égal à \verb+$b+ \\
			\hline
			\verb+$a -gt $b+ & \verb+$a+ supérieur à \verb+$b+ \\
			\hline
			\verb+$a -ge $b+ & \verb+$a+ supérieur ou égal à \verb+$b+ \\
			\hline
		\end{tabular}
		\caption{Comparaisons sur les nombres en Bash}
		\label{tab:comp_nombres}
	\end{table}
\end{frame}

\begin{frame}[fragile]{Nombres}{Exemple}
	Dans le fichier \texttt{script.sh}, on écrit :
	\medskip
	
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash

read -p 'Quel est ton age ? ' age

if [ $age -lt 18 ]
then
    echo 'Tu es mineur'
fi

if [ $age -ge 18 ]
then
    echo 'Tu es majeur'
    if [ $age -eq 18 ]
    then
        echo 'De peu !'
    fi
fi
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]
	On exécute le script :
	\medskip
	
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh \Pause
Quel est ton age ? \Pause 14 \Pause
Tu es mineur \Pause

remy@hp-remy:~/scripts$ ./script.sh \Pause
Quel est ton age ? \Pause 21 \Pause
Tu es majeur \Pause

remy@hp-remy:~/scripts$ ./script.sh \Pause
Quel est ton age ? \Pause 18 \Pause
Tu es majeur
De peu !
		\end{Verbatim}
	\end{beamercolorbox}
	\medskip
	
	\begin{alertblock}{}
		On note bien que \texttt{-lt} a vérifié que l'âge était \textbf{strictement} inférieur tandis que \texttt{-ge} a vérifié qu'il était supérieur ou égal.
	\end{alertblock}
\end{frame}

\begin{frame}[fragile]{Fichiers}{Les différents tests}
	\begin{table}
		\centering
		\begin{tabular}{|c|c|}
			\hline
			\textbf{Test et syntaxe} & \textbf{Explications} \\
			\hline\hline
			\verb+-e $nom+ & Le fichier (ou répertoire) \verb+$nom+ existe \\
			\hline
			\verb+-d $nom+ & \verb+$nom+ est un répertoire \\
			\hline
			\verb+-f $nom+ & \verb+$nom+ est un fichier \\
			\hline
			\verb+-L $nom+ & \verb+$nom+ est un lien \\
			\hline
			\verb+-r $nom+ & \verb+$nom+ est lisible \\
			\hline
			\verb+-w $nom+ & On peut écrire dans \verb+$nom+ \\
			\hline
			\verb+-x $nom+ & On peut exécuter \verb+$nom+ \\
			\hline
			\verb+$f1 -nt $f2+ & \verb+$f1+ est plus récent que \verb+$f2+ \\
			\hline
			\verb+$f1 -ot $f2+ & \verb+$f1+ est plus vieux que \verb+$f2+ \\
			\hline
		\end{tabular}
		\caption{Tests sur les fichiers en Bash}
		\label{tab:comp_fichiers}
	\end{table}
\end{frame}

\begin{frame}[fragile]{Fichiers}{Exemple}
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash

mkdir -p dossier
if [ -d dossier ]
then
    echo 'dossier est un répertoire'
    touch dossier/test.sh
    if [ -w dossier/test.sh ]
    then
        if [ -x dossier/test.sh ]
        then
            echo Fichier executable
        else
            echo 'rendre le fichier executable ...'
            chmod a+x dossier/test.sh
            echo 'Écrire dans test.sh'
            echo '#/bin/bash' > dossier/test.sh
            echo 'ls /' >> dossier/test.sh
        fi
    fi
    echo 'Exécuter dossier/test.sh'
    ./dossier/test.sh
else
    echo 'Erreur'
    exit 1
fi
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]
	Résultat :
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh \Pause
dossier est un répertoire
Fichier executable
Écrire dans test.sh
Exécuter dossier/test.sh
bin  boot  cdrom  core dev  etc  home initrd.img
initrd.img.old  lib  lib64 lost+found  media  mnt
opt  proc  root  run  sbin  srv  sys  tmp usr var  vmlinuz
		\end{Verbatim}
	\end{beamercolorbox} \Pause{}
	Regardez bien ces lignes-là :
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
if [ -w dossier/test.sh ]
    then
        if [ -x dossier/test.sh ]
        then
            echo Fichier executable
        else
            echo 'rendre le fichier executable ...'
            chmod a+x dossier/test.sh
        [\ldots]
        fi
		\end{Verbatim}
	\end{beamercolorbox}\Pause
	\begin{block}{}
		Est-on obligé d'imbriquer les \texttt{if}? Ne pourrait-on pas inverser les tests pour éviter les \texttt{else} quand c'est justement le seul cas que l'on veut gérer ?
	\end{block}
\end{frame}

\subsection{Opérateurs logiques}

\begin{frame}[fragile]{Les opérateurs logiques}
	\begin{block}{Contexte}
		Les tests que nous utilisons depuis tout à l'heure renvoient des valeurs logiques (vrai ou faux). Sur ces valeurs, on peut utiliser des {\bf opérateurs logiques}.
	\end{block}
	\begin{table}
		\centering
		\begin{tabular}{|c|c|c|}
			\hline
			\textbf{Opérateur} & \textbf{Signification} & \textbf{Vrai si \ldots} \\
			\hline\hline
			\verb+&&+ & ET & Les deux conditions sont vérifiées \\
			\hline
			\verb+||+ & OU & Une des conditions est vérifiée \\
			\hline
			\verb+!+ & NON & La condition est fausse \\
			\hline
		\end{tabular}
		\caption{Les opérateurs logiques}
		\label{tab:operateurs_logiques}
	\end{table}
	\begin{exampleblock}{}
		Pour utiliser ces opérateurs, on écrira [ !~condition~] pour NON ou [~condition1~] <opérateur> [~condition2~] pour les autres.
	\end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Exemple}
	Dans \texttt{script.sh} :
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash

age1=17
age2=19

if [ $age1 -ge 18 ] && [ $age2 -ge 18 ]
then
    echo "Les deux sont majeurs"
fi

if [ $age1 -lt 18 ] || [ $age2 -lt 18 ]
then
    echo "Un des deux n'est pas majeur"
fi

if [ ! $age1 -lt 17 ]
then
    echo "age1 >= 17"
fi
		\end{Verbatim}
	\end{beamercolorbox}
	On exécute :
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh \Pause
Un des deux n'est pas majeur
age1 >= 17
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\subsection{La structure case}

\begin{frame}[fragile]{Introduction}
	\begin{block}{Contexte}
		Lorsqu'on veut réaliser des actions différentes en fonction de la valeur d'une seule variable, on peut faire un grand \texttt{if} avec beaucoup de \texttt{elif}.
		Néanmoins, cela est assez lourd à écrire et peu lisible.

		Dans ce genre de cas, on utilisera une autre structure, le \texttt{case}, qui permet d'alléger grandement le code.
	\end{block}
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash
case $extension_ficher in
    '.sh')
        echo ``C'est un script \!''
        ;;
    '.tex')
        echo ``C'est un fichier LaTeX \!''
        ;;
    '.doc' | '.xls' | '.ppt')
        echo 'Sans déconner ?'
        ;;
    *)
        echo 'Je ne connais pas !'
        ;;
esac
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Explications}
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\vspace{-10pt}
		\begin{Verbatim}
case $exension_fichier in
		\end{Verbatim}
		\vspace{-5pt}
	\end{beamercolorbox}

	\begin{block}{}
		On teste la valeur de la variable \verb+$extension_ficher+ (que l'on suppose exister).
	\end{block} \Pause

	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\vspace{-10pt}
		\begin{Verbatim}
'.sh')
    echo ``C'est un script \!''
    ;;
		\end{Verbatim}
		\vspace{-5pt}
	\end{beamercolorbox}

	\begin{block}{}
		Si c'est \texttt{.sh}, on affiche \enquote{C'est un script !}. Les deux \texttt{;;} signifient que c'est la fin de ce qu'il faut exécuter, et on sort du case.
	\end{block} \Pause

	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\vspace{-10pt}
		\begin{Verbatim}
'.doc' | '.xls' | '.ppt')
	echo 'Sans déconner ?'
	;;
		\end{Verbatim}
		\vspace{-5pt}
	\end{beamercolorbox}

	\begin{block}{}
		On peut effectuer la même action pour plusieurs valeurs.

		\textbf{Attention} le \enquote{ou} s'écrit alors avec un seul \texttt{|}.
	\end{block}
\end{frame}

\begin{frame}[fragile]{Explications}
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\vspace{-10pt}
		\begin{Verbatim}
*)
	echo 'Je connais pas !'
	;;
		\end{Verbatim}
		\vspace{-5pt}
	\end{beamercolorbox}

	\begin{block}{}
		\enquote{\texttt{*}} est un \enquote{wildcard}, ou joker. Il capture toutes les possibilités. Ici, tout ce qui n'est pas passé par une autre condition rentre dans ce cas. C'est l'équivalent du \enquote{\texttt{else}}.
	\end{block} \Pause

	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\vspace{-10pt}
		\begin{Verbatim}
esac
		\end{Verbatim}
		\vspace{-5pt}
	\end{beamercolorbox}
	\begin{block}{}
		C'est \enquote{\texttt{case}} à l'envers !
	\end{block}
\end{frame}
