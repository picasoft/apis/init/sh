\begin{frame}{Qu'est-ce qu'une variable?}
	\begin{block}{Définition}
		Une variable est une zone de mémoire dans laquelle on stocke une valeur.
		Concrètement, on ne s'occupera pas de la partie gestion de mémoire,
		on se limitera au couple clé (nom de la variable)/valeur.
	\end{block}

	\begin{block}{Pourquoi variable?}
		On appelle cela une variable, car la valeur associée à la clé peut changer au cours de l'execution du programme. On dit qu'on peut la \textbf{réaffecter}.
	\end{block}
\end{frame}

\begin{frame}[fragile]{Affecter une variable}
	\begin{block}{Affecter}
		{\bf Affecter} une valeur à une variable, c'est dire {\it \enquote{Quand j'utiliserai le nom de cette variable, tu le remplaceras par cette valeur}}.
	\end{block}
	\begin{block}{Syntaxe}
		Pour affecter une valeur à une variable, on utilisera la syntaxe :
		\begin{center}
			{\bf\large \textcolor{red}{nom}\textcolor{blue}{=}\textcolor{myGreen}{valeur}}
		\end{center}
	\end{block}
	\begin{alertblock}{{\bf Attention!}}
		Il ne faut surtout pas mettre d'espace avant et après le signe \texttt{=}
	\end{alertblock}
	Exemple:

	Dans le fichier \texttt{script.sh} on écrit :
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash
ma_variable="Salut tout le monde"
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Affecter une variable}
	On exécute le script :
	
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ chmod +x script.sh
remy@hp-remy:~/scripts$ ./script.sh \Pause
		\end{Verbatim}
	\end{beamercolorbox}
	\medskip
	
	\begin{beamercolorbox}[rounded=true]{orangebox}
		\enquote{Attends, mais pourquoi y a rien qui s'affiche ?}
	\end{beamercolorbox} \Pause
	\medskip
	
	\begin{block}{Réponse}
		Parce que notre script ne fait qu'\textbf{affecter} une valeur à une variable!
		On ne lui a jamais demandé d'afficher quoi que ce soit !
	\end{block}
\end{frame}

\begin{frame}[fragile]{Lire une variable}
	\begin{block}{Accéder au contenu}
		Pour accéder au contenu d'une variable, il faut écrire son \textcolor{red}{\bf nom}, précédé du symbole \textcolor{red}{\bf \$}.
	\end{block}
	\medskip
	
	On modifie le fichier \texttt{script.sh} :
	\begin{beamercolorbox}[rounded=true, shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash
ma_variable="Salut tout le monde"
echo $ma_variable
		\end{Verbatim}
	\end{beamercolorbox} \Pause

	En exécutant le script, on obtient :
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh \Pause
Salut tout le monde
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Les quotes}
	\begin{block}{Explications}
		Il y a trois types de \enquote{quotes} en Bash:
		\begin{itemize}
			\item Les \enquote{simple quotes} (\verb+'+) qui affichent le contenu tel qu'il est écrit
			\item Les \enquote{double quotes} (\verb+"+) qui interprètent des variables ou des caractères d'échapement dans du texte
			\item Les \enquote{backquotes} (\verb+`+) qui interprètent le contenu comme une commande
		\end{itemize}
	\end{block}
	On écrit dans \texttt{script.sh}
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash
cmd="ls"

echo '$cmd'
echo "$cmd"
echo `$cmd`
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]
	Le résultat obtenu est :
	\medskip
	
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
$cmd
ls
helloWorld.sh script.sh
		\end{Verbatim}
	\end{beamercolorbox}
	\medskip
	
	\begin{block}{Explications}
		\begin{description}
			\item[\bf Ligne 1] L'utilisation de simple quotes n'interprète pas \verb+$cmd+ comme une variable.
			\item[\bf Ligne 2] L'utilisation de double quotes interprète \verb+$cmd+ comme une variable et affiche son contenu.
			\item[\bf Ligne 3] L'utilisation de backquotes interprète \verb+$cmd+ comme une commande et execute donc le \texttt{ls} depuis le répertoire de lancement du script.
		\end{description}
	\end{block}
	\begin{alertblock}{Quelle forme utiliser pour les variables ?}
		On préférera utiliser des double quotes (\verb+"+) quand on veut utiliser le contenu des variables, surtout si le contenu peut contenir des espaces!
	\end{alertblock}
\end{frame}

\begin{frame}[fragile]{Entrées utilisateur}
	\begin{block}{Contexte}
		L'intérêt des variables, c'est aussi de pouvoir demander à l'utilisateur de saisir des données pour rendre le programme {\bf interactif}.
	\end{block}
	\begin{block}{Utilisation}
		Pour lire une entrée utilisateur, on utilise la commande
		\begin{center}
			\texttt{read \textcolor{red}{variable}}
		\end{center}

		L'option \texttt{-p} permet d'afficher à destination de l'utilisateur.\\
		{\tiny RTFM pour plus de super options !}
	\end{block}
	Exemple (toujours dans \texttt{script.sh}) :
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
read -p 'Quel est votre nom ? ' nom
echo "Salut $nom"
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Les opérations arithmétiques}
	\begin{block}{Contexte}
		Bash ne gère pas nativement les variables comme des nombres (par exemple, \og{}\texttt{2+2}\fg{} renverra une erreur). Il faut passer par la commande \texttt{let}.
	\end{block}
	Exemple :
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash
let "a=5"
let "b=2"
let "c=a+b"
echo $c
		\end{Verbatim}
	\end{beamercolorbox}
	\medskip
	
	Quand on execute le script, on obtient :
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh \Pause
7
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]{Les opérateurs arithmétiques}
	Les opérateurs reconnus par \texttt{let} sont :
	\begin{table}
		\centering
		\begin{tabular}{|c|c|c|}
			\hline
			{\bf Symbole} & {\bf Signification} & {\bf Exemple} \\
			\hline\hline
			\texttt{+} & Addition & \texttt{5+2=7} \\
			\hline
			\texttt{-} & Soustraction & \texttt{5-2=3} \\
			\hline
			\texttt{*} & Multiplication & \texttt{5*2=10} \\
			\hline
			\texttt{/} & Division euclidienne & \texttt{5/2=2} \\
			\hline
			\texttt{**} & Puissance & \texttt{5**2=25} \\
			\hline
			\texttt{\%} & Modulo & \texttt{5\%2=1} \\
			\hline
		\end{tabular}
		\caption{Les opérateurs supportés par let}
		\label{tab:operators}
	\end{table}
	\begin{exampleblock}{Info}
		Comme pour le langage C, il est possible d'utiliser la syntaxe \verb|let "a+=3"| à la place de \verb/let "a=a+3"/ {\tiny(ou autres opérateurs).}
	\end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Les paramètres d'exécution}
	\begin{block}{Contexte}
		On a vu cette semaine qu'une commande peut recevoir un ou plusieurs paramètres, séparés par des espaces (exemple : \texttt{mv <source> <destination>}). Il en est de même pour les scripts Shell.

		Comme pour les commandes, l'ordre des paramètres est {\bf primordial} !
	\end{block}
	\begin{block}{Accéder aux paramètres}
		Lors de l'exécution, des variables sont automatiquement créées : \texttt{\$\#} contient le nombre de paramètres, \texttt{\$1} le premier, \texttt{\$2} le deuxième~\ldots et \texttt{\$9} le neuvième.
	\end{block}
	Exemple:
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash
echo "Il y a $# paramètres d'execution"
echo "Le premier paramètre est $1"
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}

\begin{frame}[fragile]
	Ce qui donnera:
	\medskip
	
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
remy@hp-remy:~/scripts$ ./script.sh test \Pause
Il y a 1 paramètres d'execution
Le premier paramètre est test \Pause
remy@hp-remy:~/scripts$ ./script.sh test toto \Pause
Il y a 2 paramètres d'execution
Le premier paramètre est test \Pause
remy@hp-remy:~/scripts$ ./script.sh \Pause
Il y a 0 paramètres d'execution
Le premier paramètre est \Pause
		\end{Verbatim}
	\end{beamercolorbox}
	\medskip
	
	\begin{beamercolorbox}[rounded=true]{orangebox}
		\enquote{Mais attends, je peux avoir que 9 paramètres ?}
	\end{beamercolorbox}
	\medskip
	
	\begin{block}{}
		Non, on peut en fait avoir plus de paramètres, bien qu'en général on utilise plutôt des fichiers de configuration quand le nombre de paramètres explose.
		On en reparlera quand on saura utiliser les boucles, pour faire ça \textit{proprement}.
	\end{block}
\end{frame}

\begin{frame}[fragile]{Les tableaux}{Créer et lire un tableau}
	\begin{block}{Contexte}
		En Bash, on peut également déclarer des tableaux. Ce sont des variables un peu spéciales, qui contiennent plusieurs valeurs référencées par un {\bf index}.
	\end{block}
	\begin{block}{Accéder à une valeur}
		Les valeurs stockées dans un tableau possèdent un \textbf{index} qui permet d'y accéder individuellement.
		La syntaxe pour lire une valeur est \verb+${nom[index]}+. On peut également utiliser \verb+${nom[@]}+ pour afficher toutes les valeurs associées à un index \textbf{numérique}.
	\end{block}
	\begin{block}{Différents types de tableaux}
		\begin{itemize}
			\item Les tableaux \textbf{numériques}, où les indices sont des nombres,
			\item Les tableaux \textbf{associatifs}, où les indices sont des mots arbitraires.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}[fragile]{Les tableaux}{Tableau numérique}
	\begin{block}{Créer un tableau numérique}
		On utilise la syntaxe \verb+tab=(el0 el1 el2)+. Dans cet exemple, le tableau contient trois éléments, accessibles aux indices \textbf{0}, 1 et 2. On peut par exemple accéder au premier élément avec \verb+${tab[0]}+.
	\end{block}
	\begin{block}{Modifier une valeur}
		La syntaxe \texttt{tab[index]} permet également de modifier une valeur, par exemple en écrivant \verb+nom[index]=valeur+.
	\end{block}
	\begin{exampleblock}{Ajouter ou supprimer une valeur}
		On peut ajouter une valeur en utilisant la syntaxe précédente, par exemple : \verb+tab[3]=el3+. Le plus pratique reste d'utiliser la syntaxe \verb|tab+=(el3 el4 el5)|, qui prend automatiquement les bons indices.
		Pour supprimer une valeur, on utilisera la syntaxe \verb+unset tab[index]+.
	\end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Les tableaux}{Tableau associatif}
	\begin{block}{Créer un tableau associatif}
		On utilise la syntaxe \verb+declare -A tab+.
	\end{block}
	\begin{block}{Opérations classiques}
		On utilise la même syntaxe que pour les tableaux numériques, excepté pour ajouter plusieurs éléments d'un seul coup avec le \verb|+=|.
		
		Et cette fois ci, les indices peuvent être des mots, ce qui est très pratique pour stocker des scores, des clé-valeurs\ldots
	\end{block}
	\begin{exampleblock}{Remarque}
		Il est théoriquement possible de mélanger tableau numérique et tableau associatif, mais franchement, c'est une mauvaise idée\ldots
	\end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Les tableaux}{Exemple}
	On ecrit dans \texttt{script.sh} :
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
		\begin{Verbatim}
#!/bin/bash

tab_num=( e0 e1 )
echo \${tab_num[0]}
echo \${tab_num[@]}
unset tab_num[0]
echo \${tab_num[*]}

declare -A tab_ass
tab_ass[foo]=jury
tab_ass[bar]=bc01
echo \${tab_ass[foo]}
echo \${tab_ass[@]}
		\end{Verbatim}
	\end{beamercolorbox}
	On exécute, et on obtient:
	\begin{beamercolorbox}[rounded=true,shadow=true]{terminal}
\vspace{-7pt}
		\begin{Verbatim}
e0
e0 e1
e1
jury
jury bc01
		\end{Verbatim}
	\end{beamercolorbox}
\end{frame}
