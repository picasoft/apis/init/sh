#!/bin/bash 

function pause {
    echo -e "\n\nAppuyez sur entrée pour continuer :"
    read
}

function saisirUser { 
    read -p "Saisir l'utilisateur : " util 
} 
 
function verifyUser { 
    if grep "^$util:" /etc/passwd > /dev/null; then 
        echo "L'utilisateur existe" 
    else 
        echo "L'utilisateur n'existe pas" 
    fi 
    pause
}

function listUsers {
    cat /etc/passwd | while read line; do
        echo $line | cut -d ':' -f 1
    done
    pause
} 
 
rep=1 
while [ "$rep" -eq 1 ]; do 
    clear
    echo -e "Menu :\n" 
    echo "1. Vérifier l'existence d'un utilisateur" 
    echo "2. Lister les utilisateurs" 
    echo -e "q. Quitter\n" 
    read -p "Votre choix ? " choix 
    case "$choix" in 
        1) 
            saisirUser 
            verifyUser ;; 
 
        2)     
            listUsers ;;
 
        q) 
            echo "Au revoir" 
            rep=0 ;; 
        *) 
            echo "Erreur de saisie" ;;
    esac 
done