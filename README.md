# Api/casoft Init Jour 3 partie 1 : scripts shell

## Description

Repo de la formation sh de l'api/casoft Init. **En cas de questions :** [remy.huet@etu.utc.fr](mailto://remy.huet@etu.utc.fr).

Pour aller plus loin : lire la [documentation de git](https://git-scm.com/docs).

En cas de remarques sur la présentation (ou de questions aussi), vous pouvez utiliser le [système d'issues](https://gitlab.utc.fr/picasoft/apis/h19/init/sh/issues).

## Présentation

Une chaîne d'intégration permet de construire automatiquement le PDF à partir du fichier .tex présent à la racine.

Après construction (uniquement sur la branche `master`), le document final est disponible [à cette adresse](https://uploads.picasoft.net/api/linux_shell.pdf).

Le chemin d'upload est défini dans le fichier [.gitlab-ci.yml](./gitlab-ci.yml).

## Contribuer !

Créez une branche, et faites une merge request ! Faites la relire par un tiers qui la valide et la merge.
N'oubliez pas d'ajouter votre nom dans la licence et dans les auteurs de `main.tex`
